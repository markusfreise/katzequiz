/************************************************************************************************/
/*
/* Spieler
*/

var _player = function(id) {
	this.id = id;
};

_player.prototype.nickname = ""; // Nickname
_player.prototype.email = ""; // E-Mail-Address
_player.prototype.score = 0; // Overall score of _player

_player.prototype.load = function(_id) {
	var _o = this;

	if(_id<=0 || _id===undefined) {
		_id = this.id;
	}
	
	data = 0;
	
	if(!_OFFLINE_MODE) {
		if(_id>0) {
			$.getJSON("http://www.katzecomic.de/2014/wp-content/themes/lineofscrimmage/"+_json_server+"/player.php",{id: _id}).done(
				function(d){
					data = d[0];
				}
			).fail(
				function(e){
					data = 0;
					badboy();
					console.log(e.responseText);
				}
			);
		}
		if(data) {
			_o.nickname = d[0].nickname;
			_o.realname = d[0].realname;
			_o.email = d[0].email;
			_o.score = d[0].score;
		}
	}else{
		_o.nickname = "offlinegame";
		_o.realname = "Offline Game";
		_o.email = "markus@freise.de";
		_o.score = 0;
	}
	
};

_player.prototype.game_list = function() {
	
	okay = true;
	
	$("#runninggames div.results").addClass("loading");
	p = this;
	$.getJSON("http://www.katzecomic.de/2014/wp-content/themes/lineofscrimmage/"+_json_server+"/game_list.php",{player: this.id}).done(
		function(d){
		
			_html = "";

			$.each(d,function(g){

				if(this.games.length>0) {
					
					_html = _html + '<h1>'+this.typ+'</h1><ul class="results list">';
	
					$.each(this.games,function(){
						_html = _html + '<li><ul class="coplayers">';
						_start_okay = false;
						$.each(this.coplayers,function(){
							_html = _html + '<li><span onClick="" class="i">'+game_status_icons[this.status]+'</span>'+(this.realname!==""?this.realname:this.nickname)+'</li>';
							_start_okay = (_start_okay)?true:(this.status==1 && this.id!=p.id);
						});
						_html = _html + '</ul>';
						_html = _html + '<span onClick="" class="date">'+this.game.date_readable+' ID:'+this.game.id+'</span>';
						switch(this.coplayers[0].status) {
							case "0":
								_html = _html + '<div class="actions"><span onClick="" href="#" class="button accept" data-func="invitation_set('+this.game.id+','+this.coplayers[0].id+',1);">Mitspielen</span><span onClick="" href="#" class="button decline" data-func="invitation_set('+this.game.id+','+this.coplayers[0].id+',2);">Ablehnen</span></div>';
								break;
							case "1":
								switch(this.game.gamestatus) {
									case "1":
										_html = _html + '<div class="actions"><span onClick="" href="#" data-func="startgame('+this.game.id+',true)" class="button accept big">Spielen</span></div>';
										break;
									case "2":
										_html = _html + '<div class="actions"><span onClick="" href="#" data-func="startgame('+this.game.id+',true)" class="button accept big">Anschauen</span></div>';
										break;
									default:
										_html = _html + '<div class="actions"><span onClick="" class="button dead">Warten, dass Spiel gestartet wird.</span></div>';
								}
								break;
							case "99":
								if(this.game.gamestatus===0) {
									_html = _html + '<div class="actions">';
									if(_start_okay) {
										_html = _html + '<span onClick="" href="#" data-func="startgame('+this.game.id+',true)" class="button accept big">Starten</span>';
									}else{
										_html = _html + '<span onClick="" class="button dead">Warten auf Mitspieler.</span>';
									}
									//_html = _html + '<span onClick="" href="#" class="button" data-func="newgame()">Hinzufügen</span></div>';
								}else{
									_html = _html + '<div class="actions"><span onClick="" href="#" data-func="startgame('+this.game.id+',true)" class="button accept big">Spielen</span></div>';
								}
								break;
						}
						_html = _html + '</li>';
					});
	
					_html = _html + '</ul>';

				}

			});
			if(_html!==""){ // Keine Spiele gefunden
				$("#runninggames div.results").html(_html);
				buttons();
				$("#runninggames div.results").removeClass("loading");
			}else{
				okay = false;
			}
		}
	).fail(
		function(e){
			badboy();
			console.log(e.responseText);
			return false;
		}
	);
	
	return okay;
	
};

_player.prototype.changepass = function() {
	
	if($("#newpasswordfrm #password_new_1").val()!=="" && $("#newpasswordfrm #password_new_2").val()!=="") {
		if($("#newpasswordfrm #password_new_1").val()==$("#newpasswordfrm #password_new_2").val()) {
			if($("#newpasswordfrm #password_new_1").val()!=$("#newpasswordfrm #password").val()) {
				idle(1);
				$.getJSON("http://www.katzecomic.de/2014/wp-content/themes/lineofscrimmage/"+_json_server+"/game_changepassword.php",{id: this.id, password: $("#newpasswordfrm #password").val(), password_new_1: $("#newpasswordfrm #password_new_1").val(),password_new_2: $("#newpasswordfrm #password_new_2").val()}
				).done(
					function(d) {
						console.log(d);
					}
				).fail(
					function(d) {
						badboy();
						msg(d.responseText);
					}
				);			
				idle(0);
			}else{
				msg("Das bisherige und neue Passwort dürfen nicht gleich sein.");
			}
		}else{
			msg("Die Passwörter stimmen nicht überein.");
		}
	}else{
		msg("Du musst alle Felder ausfüllen.");
	}
	
};

_player.prototype.countdown = function() {
	
	if(this._timer) {
		this._timer--;
		p = this;
		/*$(".second").filter(function(idx){
			return idx>Math.floor(p._timer/_TIMER_SCALE);
		}).remove();*/
	}
};

/************************************************************************************************/
/*
/* Login
*/

function logindo(id) {
	msg("");
	idle(1);
	$("#loginfrm span").addClass("disabled");
	buttons();
	$.getJSON("http://www.katzecomic.de/2014/wp-content/themes/lineofscrimmage/"+_json_server+"/player.php",{method: "login", autologin: id, login: $("#loginfrm #login").val(),passwort: $("#loginfrm #passwort").val()}
	).done(
		// done
		function(d){
			$("#loginfrm span").removeClass("disabled");
			buttons();
			idle(0);
			if(d.length>0) {
				d = d[0];
				host.id = d.id;
				host.nickname = d.nickname;
				host.load();
				$(".loggedin").removeClass("disabled");
				buttons();
				localStorage.setItem("katzequiz_host",host.id);
				eval($("#reroute").val()+"()");
			}else if(!id) {
				msg("Die Daten sind nicht korrekt.");
			}
			// store token
			$.getJSON("http://www.katzecomic.de/2014/wp-content/themes/lineofscrimmage/"+_json_server+"/notify_settoken.php",{player: d.id, token: pushToken, device: pushOS}
			).done(function(d){
				console.log("Saved device token "+pushToken);
			});
			// store token
			return false;
		}
	// fail
	).fail(
		function(e){
			badboy();
			msg(e.responseText);
			$("#loginfrm .alert").show();
			idle(0);
			return false;
		}
	);
}

function logout() {
	host = new _player();
	localStorage.setItem("katzequiz_host","");
	buttons();
	gohome();
}

function invitation_set(game_tmp,_player,_status) {
	$.getJSON("http://www.katzecomic.de/2014/wp-content/themes/lineofscrimmage/"+_json_server+"/game_invitation_set.php",{host: host.id, game: game_tmp, player: _player, status: _status}).done(
		function(d) {
			host.game_list();
			console.log("done");
		}
	).fail(
		function(d) {
			badboy();
			console.log(d.responseText);
		}
	);

}

/************************************************************************************************/
/*
/* Antworten
*/

var _answer = function() {};

_answer.prototype.buzzer   = 0;
_answer.prototype.state    = 0;
_answer.prototype.question = 0;


/************************************************************************************************/
/*
/* Spiel
*/

var _game = function() {};

var _timer_invitelist = 0;

_game.prototype.id = 0; // Database-ID of game
_game.prototype.host = 0; // Database-ID of hosting _player
_game.prototype._players_invite = new Array(); // Array of _players who were invited
_game.prototype._players_game = new Array(); // Array of _players who were invited
_game.prototype._players = new Array(); // Array of _players who were invited
_game.prototype.questions = new Array();
_game.prototype.blocked_invites = false;

// Checks if player has accepted invitation and is in game

_game.prototype.player_playing = function(p) {
	result = false;
	$.each(this._players_game,function(){
		if(this.id===p.id) {
			result = true;
		}
	});	
	return result;
}

// Gets list of players out of db

_game.prototype.searchplayers = function() {
	block = new Array();
	$.each(this._players,function(i){block.push(this.id);});
	block.push(1*host.id);
	if($("#search").val()!=="") {
		$("#newgame div.results").html("").addClass("loading");
		$.getJSON("http://www.katzecomic.de/2014/wp-content/themes/lineofscrimmage/"+_json_server+"/player.php",{search: $("#search").val()}).done(
			function(d){
				if(d.length) {
					_html = '<ul class="results list oneliner">';
					$.each(d,function(){
						if(block.indexOf(1*this.id)==-1) {
							_html = _html + '<li class="simple"><span onClick="" href="#" data-func="if(game.addinvitelist('+this.id+')){$(this).parent().remove()}">'+this.nickname+'<span onClick="" class="i">+</span></span></li>';
						}
					});
					_html = _html + '</ul>';
				}else{
					_html = '<p>Keine Ergebnisse.</p><p>Startest doch einfach ein Einzelspiel und fordere Dich selbst heraus.</p>';
				}
				$("#newgame div.results").html(_html);
				buttons();
				$("#newgame div.results").removeClass("loading");
			}
		).fail(
			function(e){
				badboy();
				console.log(e.responseText)
				$("#newgame div.results").removeClass("loading");
			}
		);
	}else{
		alert("Bitte Suchbegriff eingeben.");
	}
	return false;
}

// Updates invites view list

_game.prototype.updateinvitelist = function() {
	_html = "";
	_stop_timer = true;
	_start_okay = false;
	_me = this;
	$.getJSON("http://www.katzecomic.de/2014/wp-content/themes/lineofscrimmage/"+_json_server+"/game_invitelist.php",{game: _me.id}
	).done(
		function(d){
			$.each(d,function(){
				_html = _html + '<li class="simple"><span onClick="" href="#">'+this.nickname+'<span onClick="" class="i">'+game_status_icons[this.status]+'</span></span></li>';
				_start_okay = (_start_okay)?true:(this.status==1);
			});
			if(_start_okay) {
				$('[data-func="startgame()"]').removeClass("disabled");
			}else{
				$('[data-func="startgame()"]').addClass("disabled");
			}
			buttons();
			$("ul#players").html(_html);
		}
	).fail(
		function(e){
			badboy();
			console.log(e);
		}
	);
	_timer_invitelist = window.setTimeout(function(){_me.updateinvitelist();},2000)
}

// Adds a player to an invitelist

_game.prototype.addinvitelist = function(_id) {
	if(!this.blocked_invites) { // Is blocked while adding to avoid async problems
		this.blocked_invites = true;
		block = new Array();
		
		$.each(this._players,function(i){
			block.push(this.id);
		});
		if(block.indexOf(_id)===-1) {
			if(this._players_invite.length<3) {
				p = new _player(_id);
				game_tmp = this;
				$.getJSON("http://www.katzecomic.de/2014/wp-content/themes/lineofscrimmage/"+_json_server+"/game_addplayer.php",{host: host.id, game: game_tmp.id,player: p.id}
				).done(
					function(d){
						game_tmp.id = d.game;
						game_tmp.blocked_invites = false;
						game_tmp._players.push(p);
						localStorage.setItem("katzequiz_game",d.game);
						window.clearInterval(_timer_invitelist);
						game_tmp.updateinvitelist();
					}
				).fail(
					function(e){
						console.log(e);
						badboy();
						_game.blocked_invites = false;
						return false;
					}
				);
				this._players_game.push(p);
			}else{
				alert("Es können nur 3 Spieler eingeladen werden.");
				return false;
			}
		}else{
			console.log("Spieler wurde bereits eingeladen!");
			return false;
		}
		return true;
	}else{
		return false;
	}
}

// Setup a single game

_game.prototype.setupsinglegame = function() {
		
	game_tmp = this;

	if(!_OFFLINE_MODE) {

		$.getJSON("http://www.katzecomic.de/2014/wp-content/themes/lineofscrimmage/"+_json_server+"/game_addplayer.php",{host: host.id, game: game_tmp.id,single: 1}
		).done(
			function(d){
				game_tmp.id = d.game;
				localStorage.setItem("katzequiz_game",d.game);
				game_tmp.load(game_tmp.id,false);
			}
		).fail(
			function(e){
				console.log(e);
				badboy();
				return false;
			}
		);

	}else{ // Setup offline game

		console.log("Setting up offline game");

		questions_ids = [];

		off_game.answers = [];
		
		round = 0;

		for(i=0;i<_QUESTIONS;i++) {
			do {
				q = Math.floor(_db.length*Math.random());
			}while(questions_ids.indexOf(q)!==-1);

			tq = _db[q];
			tq.idx = i%3;
			tq.round = round;
			if((i+1)%3==0) {
				round++;
			}
			tq.question = _db[q];
			tq.id = _db[q].id;
			tq.richtig = _db[q].richtig;
			tq.antworten = [];
			tq.antworten[0] = _db[q].richtig;
			falsch = (_db[q].falsch);
			for(j=0;j<3;j++) {
				tq.antworten[j+1] = falsch[j];
			}
			tq.antworten = shuffle(tq.antworten);
			
			off_game.questions[_db[q].id] = tq;

			questions_ids.push(q);
			
		}
		
		off_game.players = [];
		off_game.players.push(new _player());
		
		off_game.load(off_game.id,false);

	}

	return true;

}

// Setup

_game.prototype.load = function(id,silent) {

	if(id || _OFFLINE_MODE) {
	
		obj = this;
		
		console.log("ID: "+id+" / Silent: "+silent);
	
		if(!silent) {
			$("#quiz div.results").addClass("loading");
		}
		
		data = 0;
		
		if(!_OFFLINE_MODE) {
			
			console.log("Loading game "+id+" ...");
	
			$.getJSON("http://www.katzecomic.de/2014/wp-content/themes/lineofscrimmage/"+_json_server+"/game_setup.php",{host: host.id, kategorie: _CAT_ONLY, game: id, cat_only: _CAT_ONLY}
			).done(
				function(d){
					console.log(d);
					data = d;
				}
			).fail(
				function(e){
					data = 0;
					idle(0);
					badboy();
					console.log(e.responseText);
				}
			);
		
		}else{

			data = off_game;
			
			data.rank      = off_game.players;

			console.log("OFFLINE MODE game.load");
			
			d = data;

		}
		
		console.log(data);
	
		if(data) { // Data found, load game now ...
		
			console.log(d);
			
			game.id = id;
			game.host = host.id;
			game.players = data.players;
			game.questions = data.questions;
			game.answers = data.answers;
			
			_html = '';

			last_round_complete = -1;
			host_next_round = -1;
			
			_right = 0;
			_wrong  = 0;

			_html = _html + '<h1 class="players_'+game.players.length+'">Aktueller Spielstand</h1><ul id="ranks" class="players_'+game.players.length+'">';

			_html_tmp = "";

			$.each(data.rank,function(){

				if(!_OFFLINE_MODE) {
					_html_tmp = _html_tmp + '<li class="rank"><ul><li class="avatar rounds"><span onClick="" class="score'+((this.id==host.id)?" host":"")+'">'+this.score+'</span><span onClick="" class="player">'+this.nickname+'</span></li>';
				}

				for(i=0;i<game.players.length;i++) {
					if(game.players[i].id==this.id) {
						p = i;
					}
				}
	
				for(r=0;r<5;r++) {
	
					_html_tmp = _html_tmp + '<li class="rounds">';
					_html_tmp = _html_tmp + '<ul>';//<li class="label">'+(r+1)+'<li>';
	
					_html_tmp = _html_tmp + '<li>';
					_answers_cnt = 0;
					_host_answers_cnt = 0;
					for(q=0;q<3;q++) {
						answer = "not";
						buzzer = 0;
						rank = 99;
						if(game.answers) {
							for(a=0;a<game.answers.length;a++) {
								if(!_OFFLINE_MODE) {
									idx = r*3+q;
								}else{
									idx = game.answers[a].question;
								}
								if(idx!=0 && game.answers[a].question == game.questions[idx].id && ((r==game.questions[idx].round && q==game.questions[idx].idx) || !_OFFLINE_MODE)) {
									_answers_cnt ++;
									_host_answers_cnt += (game.answers[a].player==host.id || _OFFLINE_MODE);
									if(game.answers[a].player == game.players[p].id) {
										answer = "wrong";
										buzzer = (game.answers[a].buzzer/1000);
										if(game.answers[a].correct==1)	{
											answer = "right";
											_right++;
										}else{
											_wrong++;
										}
										rank = game.answers[a].rank;
									}
								}
							}
						}
						_html_tmp = _html_tmp + '<span onClick="" class="'+answer+' rank_'+rank+'">'+buzzer.toFixed(2)+'</span>';
					}
					_html_tmp = _html_tmp + '</li>';
	
					if(_host_answers_cnt<3 && host_next_round==-1) {
						host_next_round = r;
					}
					console.log("Answers: "+_answers_cnt);
					if(_answers_cnt==3*game.players.length) {
						last_round_complete = r;
					}
					_html_tmp = _html_tmp + '</ul></li>';
				}
				
				_html_tmp = _html_tmp + '</ul></li>';
			
				if(_OFFLINE_MODE) {
					_html_tmp = '<li class="rank"><ul><li class="avatar rounds"><span onClick="" class="score'+((this.id==host.id)?" host":"")+'">'+_right+':'+_wrong+'</span><span onClick="" class="player">'+this.nickname+'</span></li>' + _html_tmp;
				}

			});
			
			_html = _html + _html_tmp;

			
			// _html = _html + '</li></ul>';

			$('#quiz span.playbutton').removeClass("disabled").attr("data-round",host_next_round);
			if((last_round_complete == host_next_round-1)) {
				console.log("Play next round.");
				buttons();
			}else{
				console.log("Don't play next round.");
				$('#quiz span.playbutton').addClass("disabled");
			}
			
			buttons();

			if(game.players.length>1) {
				refreshinterval_obj = window.setTimeout(function(){startgame(game.id,false)},refreshinterval*1000);
			}else{
				console.log("Single game. No auto refresh.");
			}
						
			$("#quiz div.results").html(_html).removeClass("loading");

		}

		return true;

	}
}

// Spielen

_game.prototype.play = function(round) {

	if(this.id || _OFFLINE_MODE) {
		
	
		$("#frage").html("");
		$("#frage").removeClass("go away");
		$("#frage").addClass("loading");

		data = 0;

		if(!_OFFLINE_MODE) {
			$.getJSON("http://www.katzecomic.de/2014/wp-content/themes/lineofscrimmage/"+_json_server+"/game_get_question.php",{game: this.id,player: host.id,round: round}
			).done(
				function(d){
					data = d;
				}
			).fail(
				function(d) {
					data = 0;
					badboy();
					console.log(d);
				}
			)
		}else{
			off_game.next_round_player = round;
			data = off_game;
			d = data;
		}
		
		if(data) {
			console.log("Starting round "+round);
			console.log(d);
			_html = "";
			if(round==undefined || round==d.next_round_player) {
				if(d.questions) {
					$.each(d.questions,function(){
						if(!this.answer_obj && this.question && ((this.question.round==round && this.question.idx==questions_idx) || !_OFFLINE_MODE) && _html=="") {
							questions_idx++;
							_html = _html + '<div class="content" data-question="'+this.id+'" data-round="'+round+'">';
							_html = _html + '<h1>'+this.question.kategorie+'<span onClick="" class="timer">0.00</span></h1>';
							switch(this.question.typ) {
								case "bild":
									if(!_OFFLINE_MODE) {
										_html = _html + '<p class="bild"><img src="'+this.question.bild+'"></p>';
									}else{
										_html = _html + '<p class="bild"><img src="data/bilder/'+this.question.bild+'"></p>';
									}
								case "normal":
									_html = _html + '<p class="frage '+this.question.typ+'">'+this.question.frage+'</p>';
									_html = _html + '</div><ul class="answers first nav">';
									for(i=0;i<4;i++) {
										_html = _html + '<li><span onClick="" href="#" class="button answer state_'+(this.question.antworten[i]==this.question.richtig)+'" data-func="showanswer(this,'+this.id+','+(this.question.antworten[i]==this.question.richtig)*1+','+d.next_round_player+')">'+this.question.antworten[i]+'</span></li>';
									}
									_html = _html + '</ul>';
									break;
								case "zurodnung":
									_html = _html + '<p class="frage zuorndung">'+this.question.frage+'</p>';
									_html = _html + '</div><ul class="answers first zuordnung nav">';
									for(i=0;i<4;i++) {
										_html = _html + '<li><span onClick="" href="#" class="button answer">'+this.question.paare[i][0]+'</span></li>';
									}
									_html = _html + '<li class="long"><span onClick="" class="button" href="#" data-func="showanswer(this,0,1,'+d.next_round_player+')">Bestätigen</span></li>';
									_html = _html + '</ul>';
									_html = _html + '<ul class="answers zuordnung zuzuordnen nav">';
									for(i=0;i<4;i++) {
										_html = _html + '<li class="ordli" data-ord="'+(this.question.shuffle[i]-1)+'"><span onClick="" href="#" class="button answer ord">'+this.question.paare[this.question.shuffle[i]-1][1]+'</span></li>';
									}
									_html = _html + '</ul>';
									break;
							}
						}
					});
				}
				
			}
			if(_html!=="") {
				$(".answered").removeClass("answered");
				$("#frage").html(_html);
				buttons();
				window.setTimeout(function() {
					$(".zuzuordnen").sortable({items: ".ordli"}).addClass("lock");
				},1);
				$("#frage").removeClass("loading");
				showpage("frage");
			}else{
				questions_idx = 0;
				console.log("No more questions ...");
				startgame(game.id,true);
				$("#frage").removeClass("loading");
				showpage("quiz");
			}
		}
		
	}
};

function frage_setup() {
	window.clearInterval(host._countdown)
	h = 0;
	$("#frage .button").each(function() {
		h = Math.max(h,$(this).height());
	})
	$("#frage ul.answers .button").height(h);
	window.setTimeout(function(){$("#frage div.content").height($("#frage").height()-$("ul.answers.first").height()-30)},100);
	$("#frage").addClass("go");
	host.timerstart = Date.now();
	window.setTimeout(function(){
		host._countdown = window.setInterval(function(){
			$("#frage h1 span").text(((Date.now()-host.timerstart)/1000).toFixed(2));
			if((Date.now()-host.timerstart)/1000>=_MAX_TIME_PER_QUESTION) {
				$("#frage h1 span").text((_MAX_TIME_PER_QUESTION).toFixed(2));
				window.clearInterval(host._countdown)
				showanswer(0,0,0,$(".content").data("round"));
			}
		},100);
	},350);
}

function showanswer(obj,dbid,correct,round) {

	host.timerend = Date.now();
	host.timertime = Math.min(_MAX_TIME_PER_QUESTION*1000,host.timerend-host.timerstart);
	$("#frage h1 span").text((host.timertime/1000).toFixed(2));
	window.clearInterval(host._countdown);
	$("#frage .button").unbind("click");
	

	$(obj).addClass("lock");
	
	if($(".frage").hasClass("zuorndung")) { // if order-question get state of answer first
		correct = 1;
		$(".zuzuordnen").removeClass("lock");
		for(i=0;i<4;i++) {
			if(i!=$("ul.zuzuordnen li").eq(i).data("ord")) {
				correct = 0;
			}
		}
	}

	window.setTimeout(function(){ // Set button colors

		if(!$(".frage").hasClass("zuorndung")) { // normal or picture question

			$("#frage").addClass("answered");
			$(obj).addClass("answered");

		}else {

			$("ul.zuordnung").addClass("richtigezuordnung");
			for(i=0;i<4;i++) {
				if(i!=$("ul.zuzuordnen li").eq(i).data("ord")) {
					$("ul.zuzuordnen li span").eq(i).addClass("state_false");
				}else{
					$("ul.zuzuordnen li span").eq(i).addClass("state_true");
				}
			}
			$("#frage").addClass("answered");

		}
		
		if(off_game.questions[$("#frage .content").data("question")].erlaeuterung!="") {
			$(".content").height(524);
			bild = "";
			if(off_game.questions[$("#frage .content").data("question")].erlaeuterung_bild) {
				bild = '<p><img src="data/bilder/'+off_game.questions[$("#frage .content").data("question")].erlaeuterung_bild+'"></p>';
			}
			$("p.frage").html(bild+off_game.questions[$("#frage .content").data("question")].erlaeuterung);
			font_size = 19;
			while($("p.frage").height()>524) {
				$("p.frage").css("font-size",font_size+"px");
				font_size--;
			}
		}
		
		if(correct) {
			if(phoneGapAvailable()) {
				_pg_yes.play();
			}else{
				_snd_yes.currentTime = 0;
				_snd_yes.play();
			}
		}else{
			if(phoneGapAvailable()) {
				_pg_no.play();
			}else{
				_snd_no.currentTime = 0;
				_snd_no.play();
			}
		}

	},1200*(obj!=undefined));
	

	if(!_OFFLINE_MODE) {
	
		$.getJSON("http://www.katzecomic.de/2014/wp-content/themes/lineofscrimmage/"+_json_server+"/game_set_answer.php",{game: game.id,player: host.id,question:  $("#frage .content").data("question"),state: correct, round: $("#frage .content").data("round"), buzzer: host.timertime}
		).done(function(d){
			console.log("Answer set");
			console.log(d);
			$("#frage").append('<span onClick="" class="continue"></span>');
			auto_continue = window.setTimeout(
				function(){
					$("#frage").addClass("away");
					window.setTimeout(function(){
						$("#frage").html("");
						showpage();
						game.play(round);
					},900);
				},100000
			);
			$('.continue').click(function(){
				window.clearTimeout(auto_continue);
				$("#frage").addClass("away");
				window.setTimeout(function(){
					showpage();
					game.play(round);
				},700);
			})
		}).fail(function(e){
			badboy();
			console.log(e);
		})

	}else{
		
		console.log("Runde: "+$("#frage .content").data("round"));
		
		off_game.questions[$("#frage .content").data("question")].answer_obj = 1;

		a = new _answer();

		a.buzzer = host.timertime;
		a.correct  = correct;
		a.question = $("#frage .content").data("question");
		
		off_game.answers[off_game.questions[$("#frage .content").data("question")].round*3+off_game.questions[$("#frage .content").data("question")].idx] = a;
		
		off_game.players[0].score += correct;
		
		console.log(off_game.answers);

		$("#frage").append('<span onClick="" class="continue"></span>');

		auto_continue = window.setTimeout(
			function(){
				$("#frage").addClass("away");
				window.setTimeout(function(){
					$("#frage").html("");
					showpage();
					game.play(round);
				},900);
			},100000
		);
		$('.continue').click(function(){
			window.clearTimeout(auto_continue);
			$("#frage").addClass("away");
			window.setTimeout(function(){
				showpage();
				game.play(round);
			},700);
		})
		
	}


}

/************************************************************************************************/
/*
/* Push
*/


/************************************************************************************************/
/*
/* Click functions
*/

function gohome() {
	showpage("home");
}

function badboy() {
	showpage("badboy");
}

function runninggames() {
	console.log("a"+host.game_list());
	if(host.id) {
		if(host.game_list()) {
			showpage("runninggames");
		}
	}else{
		loginform("runninggames");
	}
}

function loginform(_login_reroute) {
	$("#loginfrm input").val("");
	$("#loginfrm input#reroute").val((_login_reroute==undefined)?"runninggames":_login_reroute);
	showpage("loginform");
}

function account() {
	if(host.id) {
		showpage("account");
	}else{
		loginform("account");
	}
}

function searchplayers() {
	if(host.id) {
		game.searchplayers();
		return false;
	}else{
		loginform("searchplayers");
	}
}

function newgame() {
	if(host.id) {
		game = 0;
		game = new _game();
		showpage("newgame");
	}else{
		loginform("newgame");
	}
}

function singlegame() {
	if(host.id) {
		game = new _game();
		if(game) {
			game.setupsinglegame();
			showpage("quiz");
		}
	}
}

function startgame(id,showidle) {
	if(host.id) {
		game.id = id;
		if(game.load(id,showidle==false)) {
			showpage("quiz");
		}
	}else{
		loginform("runninggames");
	}
}

function play(id) {
	game.id = id;
	game.play($(".playbutton").attr("data-round")*1);
}

function answer() {
	showpage("score");
}

/************************************************************************************************/
/*
/* UI functions
*/

function showpage_fallback(idx) {
	
	$(".page.active").removeClass("active").hide();
	if(idx!=="") {
		$("#"+idx).addClass("active").show();
		try {
			eval(idx+"_setup()");
		}catch(err){
		}
		if(idx=="frage") {
			window.clearInterval(host._countdown)
			host.timerstart = Date.now();
			host._countdown = window.setInterval(function(){host._countdown},100);
		}
	}
}

function showpage(idx) {

	window.clearInterval(_timer_invitelist);
	window.clearTimeout(refreshinterval_obj);

	msg("");
	if(!idx || idx==undefined) {
		idx="";
	}

	if(idx!=_page && idx!=="") {

		$(".page.active").addClass("moveout");
		window.setTimeout(function(){
			$(".page.active").removeClass("active").removeClass("moveout");
			$("#"+idx).removeClass("active").removeClass("moveout").addClass("movein");
			window.setTimeout(function(){
				$(".page").removeClass("movein");
				$("#"+idx).addClass("active");
				try {
					eval(idx+"_setup()");
				}catch(err){
					// console.log(err);
					// I don'g give a ...
				}
			},500);
		},500);
		
	}else if(idx=="frage") {
		window.clearInterval(host._countdown)
		host.timerstart = Date.now();
		host._countdown = window.setInterval(function(){host._countdown},100);
	}

	_page = idx;

}

function idle(show) {
	if(show) {
		$("#idle").addClass("active");
	}else{
		$("#idle").removeClass("active");
	}
}

function buttons() {
	$('[href="#"]').each(function(){
		$(this).unbind("click");
		$(this).click(function(ev) {
			if(!$(this).hasClass("disabled")) {
				eval($(this).data("func"));	
			}
			ev.preventDefault();
		});
	});
	
}

function msg(txt) {
	$(".alert").remove();
	if(txt!=="") {
		$(".page.active").prepend('<p class="alert">'+txt+'</p>');
	}
}

/************************************************************************************************/
/*
/* Globals
*/

phoneGapAvailable = function() {
  return _phonegap;
}

var _page = "";
var _phonegap = false;

var game = new _game();
var host = new _player();

var game_status_icons = ["-","y","n"];
game_status_icons[99] = ["h"];

var pushNotification;
var pushToken = 0;
var pushOS = "iOS";

var refreshinterval = 7; // Seconds for screens to auto refresh
var refreshinterval_obj = 0;

_pg_yes = 0;
_pg_no  = 0;

document.addEventListener("deviceready",onDeviceReady,false);

if(_phonegap){
    document.addEventListener("deviceready",onDeviceReady,false);
}else{
  $(document).ready(function(){
    onDeviceReady();
  });
}

function onDeviceReady() {

	f = Math.min($(window).height()/(30+$(".page").height()),$(window).width()/(30+$(".page").width()));
		$("body").css("zoom",f);

	if(_phonegap) {

		_pg_yes = new Media("sound/41345__ivanbailey__2.mp3");
		_pg_no  = new Media("sound/131657__bertrof__game.mp3");
		
		if(StatusBar!=undefined) {
			StatusBar.overlaysWebView(true);
		}
	
		console.log("Initialising push ...");
	
		pushNotification = window.plugins.pushNotification;
	
		// Push
		
		if ( device.platform == 'android' || device.platform == 'Android' )
		{
		    /*pushNotification.register(
		        successHandler,
		        errorHandler, {
		            "senderID":"replace_with_sender_id",
		            "ecb":"onNotificationGCM"
		        });
		     */
		}
		else
		{
			console.log("Push iOS");
			
		    pushNotification.register(
		        tokenHandler = function(t) {
		        	pushToken = t;
		        },
		        errorHandler = function(e) {
		        }, {
		            "badge":"true",
		            "sound":"true",
		            "alert":"true",
		            "ecb":"onNotificationAPN"
		        });
		}	
	
	}else{
	
		_snd_no    = new Audio("sound/131657__bertrof__game-sound-wrong.mp3");
		_snd_yes   = new Audio("sound/41345__ivanbailey__2.mp3");

	}
	
	buttons();
	game = new _game();
	
	if(!_OFFLINE_MODE) {
		if(_SINGLE) {
			$("#loginfrm input#reroute").val("home");
			logindo(1);	
		}else{
			if(1*localStorage.getItem("katzequiz_host")>0) {
				logindo(1*localStorage.getItem("katzequiz_host"));	
			}else{
				$(".page").css(this.data("animate"),"100%")
				showpage("home");
			}
		}
	}else{

		host = new _player();
		host.id = 1;
		
	}
		
	console.log("Phonegap: "+phoneGapAvailable());

	FastClick.attach(document.body);
	
}

/************************************************************************************************/
/*
/* PUSH
*/


function onNotificationAPN (event) {

	if(!_SINGLE) {
	    if ( event.alert )
	    {
	        navigator.notification.alert(event.alert);
	        if(1*event.game!=0) {
		        game.load(1*event.game,false); 
		    }else{
		        runninggames();
	        }
	    }
	
	    if ( event.sound )
	    {
	        var snd = new Media(event.sound);
	        snd.play();
	    }
	
	    if ( event.badge )
	    {
	        pushNotification.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
	    }
	}
}

function shuffle(array) {
    var counter = array.length, temp, index;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

/************************************************************************************************/
/*
/* Setup
*/

_debug = 0;
questions_idx = 0;

_SINGLE = 1; // Use single/kiosk mode

_CAT_ONLY = "dsc-arminia-bielefeld"; // Only use this category of questions

_MAX_TIME              = 1800; // Seconds * 10
_MAX_TIME_PER_QUESTION = 10; // Seconds
_TIMER_SCALE           = 1;

_page = "";

_json_server = "/json-dsc";

_OFFLINE_MODE = 1; // Use offline mode
_QUESTIONS = 15; // Number of questions asked in offline mode

if(_OFFLINE_MODE) {
	_SINGLE = 1; // In offline mode only single games possible
}

off_game = new _game();



